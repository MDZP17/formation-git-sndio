<!-- .slide: data-background-image="images/git-book.png" data-background-size="600px" class="chapter" -->

## Ce qu'il faut retenir de Git

%%%

<!-- .slide: data-background-image="images/git-book.png" data-background-size="600px" class="slide" -->

### Comment écrire une histoire propre et efficace

<img src="./images/schemaCommit.JPG" width="90%">

%%%

<!-- .slide: data-background-image="images/git-book.png" data-background-size="600px" class="slide" -->

### Travailler avec un dépot distant

<img src="./images/basic-remote-workflow.png" heigth="50%" width="80%">
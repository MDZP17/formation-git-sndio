# Cloner un projet

Dans la vraie vie on n'initialise pas un nouveau projet tous les matins. On repart souvent du code du prédécesseur ou d'un collègue.<br>
Comment faire ?

Trouver l'URL de votre projet.<br>
Assurez vous que vous avez des droits de modifications sur ce projet (il faut être _developer_, _maintainer_, _owner_). <br>
Vous voulez participer à un projet sur lequel vous n'avez pas de droit ? ça on en parlera plus tard.

Placez vous là où vous voulez créer votre projet. Un clic droit dans le dossier, Git Bash Here. <br>

```
git clone <url>
```

Vous venez de créer une copie exacte du dépot distant. Tout l'historique, toute l'histoire du code écrite sur ce dépot est désormais dans un dépot local sur votre ordinateur.

Vous pouvez ajouter du code, modifier le code.
Et n'oubliez pas :

```
git add .
git commit -m "message explicite"
git pull
git push
```

Ici, pas besoin de configurer le git push. Par défaut, on push sur le dépôt _origin_ (qui ici est bien le dépot à l'origine...).

# Les branches avec git

Dans la vie d'un dépot, il peut être nécessaire et utile de travailler plus proprement en assurant une bonne hiérarchisation de la criticité du dépot : On veut pouvoir y distinguer des versions stables, des versions de test, et des fonctionnalités en cours de développement.<br>

Et ainsi, vint les branches, qui permettent de réaliser cette séparation proprement.<br>

## Git bash

Vous pouvez commencer par observer l'architecture des branches de votre dépot local :

```
git branch
```

Si vous vouliez regarder le dépot local et le depot distant :

```
git branch --all
```

Vous pouvez ensuite décider de vous déplacer sur une autre branche en appliquant la syntaxe suivante :

```
git checkout NOMBRANCHE
```

Essayez de vous déplacer sur la branche master

### Cycle de vie de branches filles

La création d'une branche crée une branche au même état que la branche que vous possédez

Créez une branche et déplacez vous :

```
git branch -c "branche"
git checkout branche
```

ou

```
git checkout -b "branche"
```

